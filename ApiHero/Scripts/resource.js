﻿var Resource = function (resource) {
    this.resource = resource;
    this.getAll = function () {
        return $.ajax({ url: '/api/' + this.resource });
    };
    this.create = function (objectDto) {
        return $.ajax({
            type: "POST",
            url: '/api/' + this.resource,
            data: objectDto
        });
    };
    this.delete = function (Id) {
        return $.ajax({
            type: "DELETE",
            url: '/api/' + this.resource + '/' + Id
        });
    };

    this.update = function (Id, objectDto) {
        
        return $.ajax({
            type: "PUT",
            url: '/api/' + this.resource + '/' + Id,
            data: {Id : Id,  Hero : objectDto}
        });
    };
};
var Hero = new Resource('Heroes');