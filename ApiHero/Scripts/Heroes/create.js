﻿$(document).ready(function () {
    $('#save').click(function () {
        var HeroDto = {
            Name: $('#HeroName').val()  
        };
        
        Hero.create(HeroDto).done(function(hero){
            var rows = '';
            rows += '<tr row_id=' + hero.Id + '>';
            rows += '<td><a class="delete" id=' + hero.Id + ' href="#">Eliminar</a></td>';
            rows += '<td><a class="edit" id=' + hero.Id + ' href="#">Save</a></td>';
            rows += '<td contenteditable="true" class="edit" id=' + hero.Id + '>' + hero.Name + '</td>';
            rows += '</tr>';
            $('#heroTable').append(rows);
            $("#HeroName").val("");
        }).fail(function (error) {
            console.log(error);
            alert("Ha ocurrido un error");
        });
    });
});
