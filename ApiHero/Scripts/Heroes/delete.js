﻿$(document).ready(function () {
    $(document).on("click", ".delete", function (e) {
        e.preventDefault();
        var id = $(this).attr('id');
        Hero.delete(id).done(function () {
            $('tr[row_id=' + id + ']').remove();
        }).fail(function (error) {
            console.log(error);
            alert("Ha ocurrido un error");
        });

    });
});