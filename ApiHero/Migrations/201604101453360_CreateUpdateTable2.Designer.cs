// <auto-generated />
namespace ApiHero.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class CreateUpdateTable2 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(CreateUpdateTable2));
        
        string IMigrationMetadata.Id
        {
            get { return "201604101453360_CreateUpdateTable2"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
