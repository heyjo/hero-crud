namespace ApiHero.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateUpdateTable1 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Heros", newName: "Heroes");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Heroes", newName: "Heros");
        }
    }
}
